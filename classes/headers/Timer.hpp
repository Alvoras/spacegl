#ifndef TIMER_HPP_INCLUDED
#define TIMER_HPP_INCLUDED
#include <iostream>
#include <ctime>

#include "../headers/BlinkingText.hpp"
#include "../../headers/config.hpp"
#include "../../headers/toolbox.hpp"

using namespace std;

class Timer{
  public:
    Timer();
    static Timer* getInstance();
    void start();
    void pause();
    void resume();
    void update();
    void hide();
    void show();
    int getPauseState();
    void blink();

  private:
    static Timer timerInstance;
    long int startedAt;
    long int pausedAt;
    long int duration;
    long int pauseDuration;
    long int pauseTotal;
    bool isVisible;
    BlinkingText *text = new BlinkingText((std::string)"0", 300, GUI_CYAN_R, GUI_CYAN_G, GUI_CYAN_B);
    std::string strDuration;
    bool isPaused;
    void updateDuration();
    void draw();
};


#endif // TIMER_HPP_INCLUDED
