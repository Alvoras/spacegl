#ifndef Screen_HPP_INCLUDED
#define Screen_HPP_INCLUDED
#include "../headers/BlinkingText.hpp"
#include "../../headers/config.hpp"
#include "../../headers/geometry.hpp"

class Screen{
  public:
    Screen(std::string title, long int interval, float r, float g, float b);
    void show();
    void hide();
    bool getIsVisible();
    void draw();
    void drawBackground();

  private:
    // 500 ms blink interval
    BlinkingText *text;
    bool isVisible;

};


#endif // Screen_HPP_INCLUDED
