#ifndef LIGHT_HPP_INCLUDED
#define LIGHT_HPP_INCLUDED

class Light{
  public:
    Light(GLenum $lightId, GLenum $type, const GLfloat *$color, const GLfloat *$pos);
    void display();
    void setMaterial(GLenum $face, GLenum $name, const GLfloat *$param);

  protected:
    const GLfloat *color;
    const GLfloat *pos;
    GLenum lightId;
    GLenum type;
    GLenum id;
    int materialCnt;
    GLenum matFace[5];
    GLenum matName[5];
    const GLfloat *matParam[5];
};


#endif // LIGHT_HPP_INCLUDED
