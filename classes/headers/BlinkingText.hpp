#ifndef BLINKINGTEXT_HPP_INCLUDED
#define BLINKINGTEXT_HPP_INCLUDED

#include "iostream"

#include "../../headers/toolbox.hpp"
#include "../headers/Text.hpp"

using namespace std;

class BlinkingText : public Text{
  public:
    BlinkingText(std::string $content, long int interval, float r, float g, float b);
    void start();
    void stop();
    void blink(float x, float y, float z);

  private:
    void toggleState();
    long int blinkInterval;
    // Master switch
    bool isActive;
    // Blink switch
    bool isVisible;
    long int lastBlink;
};


#endif // BLINKINGTEXT_HPP_INCLUDED
