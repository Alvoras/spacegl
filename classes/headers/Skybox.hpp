#ifndef SKYBOX_HPP_INCLUDED
#define SKYBOX_HPP_INCLUDED

#include <SOIL.h>

#define GL_CLAMP_TO_EDGE 0x812F
#define SKY_DISTANCE 200.0f

class Skybox{
  public:
    Skybox();
    static Skybox *getInstance();
    void draw(float x, float y, float z, float width, float height, float length);

  private:
    static Skybox skyInstance;
    // Crash the game at startup ??
    GLuint texture[6];
};


#endif // SKYBOX_HPP_INCLUDED
