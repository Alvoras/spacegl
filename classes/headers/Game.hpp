#ifndef Game_HPP_INCLUDED
#define Game_HPP_INCLUDED
#include "../headers/BlinkingText.hpp"
#include "../headers/Screen.hpp"
#include "../../headers/config.hpp"

class Game{
  public:
    Game();
    static Game* getInstance();
    bool getPauseState();
    void togglePause();
    unsigned int getFrameDelay();
    Screen *getPauseScreen();
    void setIsPresenting(bool val);
    bool getIsPresenting();

  private:
    static Game gameInstance;
    bool isPaused;
    bool isPresenting;
    Screen *pauseScreen;
    unsigned int frameDelay;
    void pause();
    void resume();
};


#endif // Game_HPP_INCLUDED
