#ifndef GUI_HPP_INCLUDED
#define GUI_HPP_INCLUDED
#include "../headers/Text.hpp"
#include "../headers/BlinkingText.hpp"
#include "../../headers/config.hpp"

class GUI{
  public:
    GUI();
    static GUI* getInstance();
    float getIndicatorH();
    float getIndicatorW();
    void update();
    void show();
    bool getIsVisible();
    void setWarning(Warning id, Warning mode);
    void startTimer();
    void showTimer();
    void hideTimer();

    void showGo();
    void hideGo();

  private:
    // 500 ms blink interval
    BlinkingText *warningLeft = new BlinkingText("Impossible d'aller plus à gauche", 500, GUI_RED_R, GUI_RED_G, GUI_RED_B);
    BlinkingText *warningRight = new BlinkingText("Impossible d'aller plus à droite", 500, GUI_RED_R, GUI_RED_G, GUI_RED_B);
    BlinkingText *warningBot = new BlinkingText("Impossible de descendre plus", 500, GUI_RED_R, GUI_RED_G, GUI_RED_B);
    BlinkingText *warningTop = new BlinkingText("Impossible de monter plus", 500, GUI_RED_R, GUI_RED_G, GUI_RED_B);
    BlinkingText *go = new BlinkingText("GO !", 100, GUI_CYAN_R, GUI_CYAN_G, GUI_CYAN_B);
    static GUI guiInstance;
    float indicatorH;
    float indicatorW;
    bool isVisible;
    void drawShipDamages();
    void drawGUITriangle(float x, float y, float z, float h, bool reverse, bool isHit);
    void drawThrottleIndicator();
    void drawThrottleRectanle(float x, float y, float z);
};


#endif // GUI_HPP_INCLUDED
