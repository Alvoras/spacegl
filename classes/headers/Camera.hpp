#ifndef CAMERA_HPP_INCLUDED
#define CAMERA_HPP_INCLUDED

enum{X, Y, Z};
enum{INC, DEC};
enum{FREE_VIEW, CENTRAL_VIEW};

class Camera{
  public:
    Camera();
    static Camera* getInstance();
    void update();
    GLdouble getR();
    GLdouble getAlpha();
    GLdouble getBeta();
    void unaryEye(int id, int mode);
    void unaryFocus(int id, int mode);
    GLdouble getFocus(int id);
    void unaryUp(int id, int mode);
    GLdouble getUp(int id);
    GLdouble getEye(int id);
    void setEye(int id, GLdouble val);
    void setFocus(int id, GLdouble val);
    void setUp(int id, GLdouble val);
    void lookAt();
    void setViewMode(int id);
    int view = CENTRAL_VIEW;
    GLdouble getEyeModifier(int id);
    void setEyeModifier(int id, GLdouble val);

    float cockpitUp();
    float cockpitForward();

  private:
    float presentDropIn();
    float presentBack();
    float presentFront();
    float presentDropFront();
    static Camera camInstance;
    GLdouble eyeX = 0.0;
    GLdouble eyeY = 0.0;
    GLdouble eyeZ = 0.0;
    GLdouble eyeXModifier = 0.0;
    GLdouble eyeYModifier = 0.0;
    GLdouble eyeZModifier = 0.0;
    GLdouble focusX = 0.0;
    GLdouble focusY = 0.0;
    GLdouble focusZ = 0.0;
    GLdouble upX = 0.0;
    GLdouble upY = 1.0;
    GLdouble upZ = 0.0;
    GLdouble eyeX1 = 0.0, eyeY1 = 0.0, eyeZ1 = 0.0;
    GLdouble alpha = 0, beta = 0;
    GLdouble r = 5.0;
};


#endif // CAMERA_HPP_INCLUDED
