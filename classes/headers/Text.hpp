#ifndef TEXT_HPP_INCLUDED
#define TEXT_HPP_INCLUDED

#include "iostream"

using namespace std;

class Text{
  public:
    Text(std::string $content,float r, float g, float b);
    void draw(double x, double y, double z);
    void setContent(std::string);
    std::string getContent();

  protected:
    std::string content;
    float r;
    float g;
    float b;
};


#endif // TEXT_HPP_INCLUDED
