#ifdef __APPLE__
  #include <GLUT/glut.h>
#elif __linux__
  #include <X11/Xlib.h>
  #include <GL/gl.h>
  #include <GL/glut.h>
  #include <GL/glx.h>
#elif _WIN32
  #include <windows.h>
  #include <GL/glut.h>
  #include <SOIL.h>
#else
  #warning "OS or compiler not supported"
#endif

#include <ctime>

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "../../headers/config.hpp"
#include "../headers/Camera.hpp"
#include "../headers/Skybox.hpp"

Skybox Skybox::skyInstance;

Skybox::Skybox(){
    texture[0] = SOIL_load_OGL_texture("textures/skybox/front.jpg", SOIL_LOAD_AUTO,SOIL_CREATE_NEW_ID,SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
    texture[1] = SOIL_load_OGL_texture("textures/skybox/back.jpg", SOIL_LOAD_AUTO,SOIL_CREATE_NEW_ID,SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
    texture[2] = SOIL_load_OGL_texture("textures/skybox/up.jpg", SOIL_LOAD_AUTO,SOIL_CREATE_NEW_ID,SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
    texture[3] = SOIL_load_OGL_texture("textures/skybox/down.jpg", SOIL_LOAD_AUTO,SOIL_CREATE_NEW_ID,SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
    texture[4] = SOIL_load_OGL_texture("textures/skybox/left.jpg", SOIL_LOAD_AUTO,SOIL_CREATE_NEW_ID,SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
    texture[5] = SOIL_load_OGL_texture("textures/skybox/right.jpg", SOIL_LOAD_AUTO,SOIL_CREATE_NEW_ID,SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
}

Skybox *Skybox::getInstance(){
  return &skyInstance;
}

void Skybox::draw(float x, float y, float z, float width, float height, float length){
  	// Center the Skybox around the given x,y,z position
  	x = x - width  / 2;
  	y = y - height / 2;
  	z = z - length / 2;
    Camera *camera = Camera::getInstance();

    float eyeX = camera->getEye(X);
    float eyeY = camera->getEye(Y);
    float eyeZ = camera->getEye(Z);

    glDisable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);

    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

    // Face
    glBindTexture(GL_TEXTURE_2D, texture[1]);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3f( SKY_DISTANCE + eyeX, -SKY_DISTANCE + eyeY, -SKY_DISTANCE + eyeZ);
    glTexCoord2f(1, 0);
    glVertex3f(-SKY_DISTANCE + eyeX, -SKY_DISTANCE + eyeY, -SKY_DISTANCE + eyeZ);
    glTexCoord2f(1, 1);
    glVertex3f(-SKY_DISTANCE + eyeX, SKY_DISTANCE + eyeY, -SKY_DISTANCE + eyeZ);
    glTexCoord2f(0, 1);
    glVertex3f( SKY_DISTANCE + eyeX, SKY_DISTANCE + eyeY, -SKY_DISTANCE + eyeZ);
    glEnd();

    // Render the left quad
    glBindTexture(GL_TEXTURE_2D, texture[LEFT]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3f( SKY_DISTANCE + eyeX, -SKY_DISTANCE + eyeY, SKY_DISTANCE + eyeZ);
    glTexCoord2f(1, 0);
    glVertex3f( SKY_DISTANCE + eyeX, -SKY_DISTANCE + eyeY, -SKY_DISTANCE + eyeZ);
    glTexCoord2f(1, 1);
    glVertex3f( SKY_DISTANCE + eyeX, SKY_DISTANCE + eyeY, -SKY_DISTANCE + eyeZ);
    glTexCoord2f(0, 1);
    glVertex3f( SKY_DISTANCE + eyeX, SKY_DISTANCE + eyeY, SKY_DISTANCE + eyeZ);
    glEnd();

    // Render the back quad
    glBindTexture(GL_TEXTURE_2D, texture[BACK]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3f(-SKY_DISTANCE + eyeX, -SKY_DISTANCE + eyeY, SKY_DISTANCE + eyeZ);
    glTexCoord2f(1, 0);
    glVertex3f( SKY_DISTANCE + eyeX, -SKY_DISTANCE + eyeY, SKY_DISTANCE + eyeZ);
    glTexCoord2f(1, 1);
    glVertex3f( SKY_DISTANCE + eyeX, SKY_DISTANCE + eyeY, SKY_DISTANCE + eyeZ);
    glTexCoord2f(0, 1);
    glVertex3f(-SKY_DISTANCE + eyeX, SKY_DISTANCE + eyeY, SKY_DISTANCE + eyeZ);

    glEnd();

    // Render the right quad
    glBindTexture(GL_TEXTURE_2D, texture[RIGHT]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);
    glVertex3f(-SKY_DISTANCE + eyeX, -SKY_DISTANCE + eyeY, -SKY_DISTANCE + eyeZ);
    glTexCoord2f(1, 0);
    glVertex3f(-SKY_DISTANCE + eyeX, -SKY_DISTANCE + eyeY, SKY_DISTANCE + eyeZ);
    glTexCoord2f(1, 1);
    glVertex3f(-SKY_DISTANCE + eyeX, SKY_DISTANCE + eyeY, SKY_DISTANCE + eyeZ);
    glTexCoord2f(0, 1);
    glVertex3f(-SKY_DISTANCE + eyeX, SKY_DISTANCE + eyeY, -SKY_DISTANCE + eyeZ);
    glEnd();

    // Render the top quad
    glBindTexture(GL_TEXTURE_2D, texture[TOP]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBegin(GL_QUADS);
    glTexCoord2f(0, 1);
    glVertex3f(-SKY_DISTANCE + eyeX, SKY_DISTANCE + eyeY, -SKY_DISTANCE + eyeZ);
    glTexCoord2f(0, 0);
    glVertex3f(-SKY_DISTANCE + eyeX, SKY_DISTANCE + eyeY, SKY_DISTANCE + eyeZ);
    glTexCoord2f(1, 0);
    glVertex3f( SKY_DISTANCE + eyeX, SKY_DISTANCE + eyeY, SKY_DISTANCE + eyeZ);
    glTexCoord2f(1, 1);
    glVertex3f( SKY_DISTANCE + eyeX, SKY_DISTANCE + eyeY, -SKY_DISTANCE + eyeZ);
    glEnd();

    // Render the bottom quad

    glBindTexture(GL_TEXTURE_2D, texture[BOTTOM]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    glBegin(GL_QUADS);
    glTexCoord2f(0, 1);
    glVertex3f(-SKY_DISTANCE + eyeX, -SKY_DISTANCE + eyeY, SKY_DISTANCE + eyeZ);
    glTexCoord2f(0, 0);
    glVertex3f(-SKY_DISTANCE + eyeX, -SKY_DISTANCE + eyeY, -SKY_DISTANCE + eyeZ);
    glTexCoord2f(1, 0);
    glVertex3f(SKY_DISTANCE + eyeX, -SKY_DISTANCE + eyeY, -SKY_DISTANCE + eyeZ);
    glTexCoord2f(1, 1);
    glVertex3f(SKY_DISTANCE + eyeX, -SKY_DISTANCE + eyeY, SKY_DISTANCE + eyeZ);
    glEnd();}
