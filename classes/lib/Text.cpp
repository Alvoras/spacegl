#ifdef __APPLE__
  #include <GLUT/glut.h>
#elif __linux__
  #include <X11/Xlib.h>
  #include <GL/gl.h>
  #include <GL/glut.h>
  #include <GL/glx.h>
#elif _WIN32
  #include <windows.h>
  #include <GL/glut.h>
#else
  #warning "OS or compiler not supported"
#endif

#include <ctime>

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "../../headers/config.hpp"
#include "../headers/Text.hpp"

Text::Text(std::string $content, float $r, float $g, float $b){
  content = $content;
  r = $r;
  g = $g;
  b = $b;
}

void Text::draw(double x, double y, double z){
  unsigned i = 0;

  glColor3f(r, g, b);
  glRasterPos3f(x, y, z);
  for (i = 0; i < content.length(); i++) {
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, content[i]);
  }

  glColor3f(GUI_CYAN_R, GUI_CYAN_G, GUI_CYAN_B);
}

void Text::setContent(std::string $content){
  content = $content;
}

std::string Text::getContent(){
  return content;
}
