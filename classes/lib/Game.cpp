#ifdef __APPLE__
  #include <GLUT/glut.h>
#elif __linux__
  #include <X11/Xlib.h>
  #include <GL/gl.h>
  #include <GL/glut.h>
  #include <GL/glx.h>
#elif _WIN32
  #include <windows.h>
  #include <GL/glut.h>
#else
  #warning "OS or compiler not supported"
#endif

#include <stdlib.h>
#include <stdio.h>

#include "../../headers/toolbox.hpp"
#include "../../headers/config.hpp"
#include "../headers/Screen.hpp"
#include "../headers/Timer.hpp"
#include "../headers/Game.hpp"

Game Game::gameInstance;

Game::Game(){
  pauseScreen = new Screen((std::string)"Pause", 300, 0.8, 0.8, 0.8);
  isPaused = false;
  isPresenting = true;
  frameDelay = FRAME_DELAY;
}

Game* Game::getInstance(){
   return &gameInstance;
}

bool Game::getIsPresenting(){
  return isPresenting;
}

void Game::setIsPresenting(bool val){
  isPresenting = val;
}

bool Game::getPauseState(){
  return isPaused;
}

Screen *Game::getPauseScreen(){
  return pauseScreen;
}

void Game::pause(){
  Timer* timer = Timer::getInstance();
  timer->pause();
  pauseScreen->show();
  isPaused = true;
}

void Game::resume(){
  Timer* timer = Timer::getInstance();
  pauseScreen->hide();
  timer->resume();
  isPaused = false;
}

void Game::togglePause() {
  if (isPaused) {
    resume();
  }else{
    pause();
  }
}

unsigned int Game::getFrameDelay() {
  return frameDelay;
}
