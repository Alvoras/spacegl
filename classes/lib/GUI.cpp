#ifdef __APPLE__
  #include <GLUT/glut.h>
#elif __linux__
  #include <X11/Xlib.h>
  #include <GL/gl.h>
  #include <GL/glut.h>
  #include <GL/glx.h>
#elif _WIN32
  #include <windows.h>
  #include <GL/glut.h>
#else
  #warning "OS or compiler not supported"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "../../headers/toolbox.hpp"
#include "../../headers/config.hpp"
#include "../headers/Game.hpp"
#include "../headers/Text.hpp"
#include "../headers/BlinkingText.hpp"
#include "../headers/Ship.hpp"
#include "../headers/GUI.hpp"
#include "../headers/Timer.hpp"
#include "../headers/Camera.hpp"

extern float _tmpGlobal1;

GUI GUI::guiInstance;

GUI::GUI(){
  isVisible = false;
  indicatorH = 0.04;
  indicatorW = 0.08;
}

void GUI::show(){
  isVisible = true;
}

GUI* GUI::getInstance(){
   return &guiInstance;
}

void GUI::hideGo(){
  go->stop();
}

void GUI::showGo(){
  go->start();
}

void GUI::showTimer() {
  Timer *timer = Timer::getInstance();
  timer->show();
}

void GUI::hideTimer() {
  Timer *timer = Timer::getInstance();
  timer->hide();
}

void GUI::update(){
  Camera *camera = Camera::getInstance();
  Game *game = Game::getInstance();
  float eyeX = camera->getEye(X);
  float eyeY = camera->getEye(Y);
  float eyeZ = camera->getEye(Z);

  Timer *timer = Timer::getInstance();

  if (game->getPauseState()) {
    timer->blink();
    return;
  }

  if (!isVisible) {
    return;
  }

  drawThrottleIndicator();
  drawShipDamages();
  timer->update();
  go->blink(eyeX-0.3, eyeY+2.2, eyeZ-2);
  warningTop->blink(eyeX-0.7, eyeY+2.2, eyeZ-5);
  warningBot->blink(eyeX-1, eyeY+2.2, eyeZ-5);
  warningLeft->blink(eyeX-1, eyeY+2.2, eyeZ-5);
  warningRight->blink(eyeX-1, eyeY+2.2, eyeZ-5);
}

bool GUI::getIsVisible(){
  return isVisible;
}

void GUI::startTimer() {
  Timer *timer = Timer::getInstance();

  timer->start();
}

void GUI::setWarning(Warning id, Warning mode){
  // if (id == Warning::TOP) {
  //   if (mode == Warning::START) {
  //     warningTop->start();
  //   }else{
  //     warningTop->stop();
  //   }
  // }else if (id == Warning::BOT) {
  //   if (mode == Warning::START) {
  //     warningTop->start();
  //   }else{
  //     warningTop->stop();
  //   }
  // }
  switch (id) {
    case Warning::TOP:
      if (mode == Warning::START) {
        warningTop->start();
      }else{
        warningTop->stop();
      }
      break;
    case Warning::BOT:
      if (mode == Warning::START) {
        warningBot->start();
      }else{
        warningBot->stop();
      }
      break;
    case Warning::LEFT:
      if (mode == Warning::START) {
        warningLeft->start();
      }else{
        warningLeft->stop();
      }
      break;
    case Warning::RIGHT:
      if (mode == Warning::START) {
        warningRight->start();
      }else{
        warningRight->stop();
      }
      break;

    default:

      break;
  }
}

void GUI::drawThrottleRectanle(float x, float y, float z) {
  glBegin(GL_QUADS);
  glColor3f(0.46,1.0,0.94); // Electric cyan
  glNormal3f(0,0,1);
  glVertex3f(x, y, z);
  glVertex3f(x, y+indicatorH, z);
  glVertex3f(x+indicatorW, y+indicatorH, z);
  glVertex3f(x+indicatorW, y, z);
  glEnd();
}

void GUI::drawThrottleIndicator() {
  int i = 0;
  Camera *camera = Camera::getInstance();
  Ship *ship = Ship::getInstance();
  float speed = ship->getSpeedZ();
  int speedLevel = floor(speed*10)+1;
  float eyeX = camera->getEye(X);
  float eyeY = camera->getEye(Y);
  float eyeZ = camera->getEye(Z);

  glPushMatrix ();
  for (i = 0; i < speedLevel; i++) {
    drawThrottleRectanle(eyeX-3.5, eyeY-2+((indicatorH*2)*i), eyeZ-5);
  }
  glPopMatrix ();
}

void GUI::drawGUITriangle(float x, float y, float z, float h, bool reverse, bool isHit) {
  float ySize = 0;
  float mult = 1.3;

  float color[3];

  if (isHit) {
    color[0] =  GUI_RED_R;
    color[1] =  GUI_RED_G;
    color[2] =  GUI_RED_B;
  }else{
    color[0] =  GUI_CYAN_R;
    color[1] =  GUI_CYAN_G;
    color[2] =  GUI_CYAN_B;
  }

  if (reverse == 1) {
    mult = 1.7;
    ySize = y-(h*mult);
  }else{
    ySize = y+(h*mult);
  }

  glBegin(GL_TRIANGLES);
  glColor3f(color[0], color[1], color[2]); // Electric cyan
  glVertex3f(x, y, z);
  glVertex3f(x-(h*0.5), ySize, z);
  glVertex3f(x-h, y, z);
  glEnd();
}

void GUI::drawShipDamages() {
  Camera *camera = Camera::getInstance();
  Ship *ship = Ship::getInstance();
  float eyeX = camera->getEye(X);
  float eyeY = camera->getEye(Y);
  float eyeZ = camera->getEye(Z);

  bool coreStatus = ship->getCoreStatus();
  bool leftWingStatus = ship->getLeftWingStatus();
  bool rightWingStatus = ship->getRightWingStatus();

  glPushMatrix ();
    // Draw ship core
    // Draw ship left
    // Draw ship right
    drawGUITriangle(eyeX-4, eyeY-1.7, eyeZ-5, 0.4, false, coreStatus);
    drawGUITriangle(eyeX-4.25, eyeY-1.8, eyeZ-5, 0.4, true, leftWingStatus);
    drawGUITriangle(eyeX-3.75, eyeY-1.8, eyeZ-5, 0.4, true, rightWingStatus);
  glPopMatrix ();
}

float GUI::getIndicatorH(){
  return indicatorH;
}

float GUI::getIndicatorW(){
  return indicatorW;
}
