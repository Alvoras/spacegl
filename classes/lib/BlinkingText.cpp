#ifdef __APPLE__
  #include <GLUT/glut.h>
#elif __linux__
  #include <X11/Xlib.h>
  #include <GL/gl.h>
  #include <GL/glut.h>
  #include <GL/glx.h>
#elif _WIN32
  #include <windows.h>
  #include <GL/glut.h>
#else
  #warning "OS or compiler not supported"
#endif

#include <ctime>

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "../headers/Camera.hpp"
#include "../headers/Text.hpp"
#include "../headers/BlinkingText.hpp"

BlinkingText::BlinkingText(std::string content, long int interval, float r, float g, float b) : Text::Text(content, r, g, b){
  blinkInterval = interval;
  isActive = false;
  lastBlink = 0;
  isVisible = false;
}

void BlinkingText::start(){
  lastBlink = std::clock();
  isActive = true;
  isVisible = true;
}

void BlinkingText::stop(){
  lastBlink = std::clock();
  isActive = false;
  isVisible = false;
}

void BlinkingText::blink(float x, float y, float z) {
  clock_t now = std::clock();

  if (isActive && now >= lastBlink+blinkInterval) {
    toggleState();
  }

  if (isVisible) {
    draw(x, y, z);
  }
}

void BlinkingText::toggleState() {
  isVisible = (isVisible)?false:true;
  lastBlink = std::clock();
}
