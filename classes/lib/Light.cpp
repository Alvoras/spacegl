#ifdef __APPLE__
  #include <GLUT/glut.h>
#elif __linux__
  #include <X11/Xlib.h>
  #include <GL/gl.h>
  #include <GL/glut.h>
  #include <GL/glx.h>
#elif _WIN32
  #include <windows.h>
  #include <GL/glut.h>
#else
  #warning "OS or compiler not supported"
#endif

#include <ctime>

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "../../headers/config.hpp"
#include "../headers/Light.hpp"

Light::Light(GLenum $lightId, GLenum $type, const GLfloat *$color, const GLfloat *$pos){
  id = $lightId;
  type = $type;
  color = $color;
  pos = $pos;
  materialCnt = 0;
}

void Light::display(){
  int i = 0;
  glLightfv(id, type, color);
  // glMaterialfv(GL_FRONT, GL_EMISSION, mat_emission);
  // glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);

  for (i = 0; i < materialCnt; i++) {
    glMaterialfv(matFace[i], matName[i],matParam[i]);
  }

  glPushMatrix();
    glTranslatef(pos[0],pos[1], pos[2]);
    glutWireCube(2);
  glPopMatrix();

  glLightfv(id, GL_POSITION, pos);
}

void Light::setMaterial(GLenum $face, GLenum $name, const GLfloat *$param){
  matFace[materialCnt] = $face;
  matName[materialCnt] = $name;
  matParam[materialCnt] = $param;
  materialCnt++;
}
