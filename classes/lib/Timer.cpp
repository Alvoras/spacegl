#ifdef __APPLE__
  #include <GLUT/glut.h>
#elif __linux__
  #include <X11/Xlib.h>
  #include <GL/gl.h>
  #include <GL/glut.h>
  #include <GL/glx.h>
#elif _WIN32
  #include <windows.h>
  #include <GL/glut.h>
#else
  #warning "OS or compiler not supported"
#endif

#include <ctime>

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "../headers/Game.hpp"
#include "../headers/Timer.hpp"
#include "../headers/Camera.hpp"
#include "../../headers/toolbox.hpp"

Timer Timer::timerInstance;

Timer::Timer(){
    startedAt = 0;
    pausedAt = 0;
    duration = 0;
    isPaused = true;
    pauseDuration = 0;
    pauseTotal = 0;
    isVisible = true;
}

 Timer* Timer::getInstance() {
    return &timerInstance;
 }

void Timer::hide(){
  isVisible = false;
}

void Timer::show(){
  isVisible = true;
}

void Timer::blink(){
  Camera *camera = Camera::getInstance();
  float eyeX = camera->getEye(X);
  float eyeY = camera->getEye(Y);
  float eyeZ = camera->getEye(Z);

  text->blink(eyeX+4, eyeY+2.2, eyeZ-5);
}

void Timer::pause() {
  text->start();
  isPaused = true;
  pausedAt = std::clock();
}

void Timer::resume() {
  text->stop();
  isPaused = false;
  pauseDuration = std::clock() - pausedAt;
  pauseTotal += pauseDuration;
}

void Timer::start() {
  isPaused = false;
  startedAt = std::clock();
  pausedAt = startedAt;
  pauseTotal = 0;
}

void Timer::draw() {
  if (!isVisible) {
    return;
  }
  Camera *camera = Camera::getInstance();
  float eyeX = camera->getEye(X);
  float eyeY = camera->getEye(Y);
  float eyeZ = camera->getEye(Z);

  text->draw(eyeX+4, eyeY+2.2, eyeZ-5);

  // glRasterPos3f(eyeX+4, eyeY+2.2, eyeZ-5);
  // for (i = 0; i < strDuration.length(); i++) {
  //   glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, strDuration[i]);
  // }
}

void Timer::updateDuration(){
  long int end = std::clock();

  std::string timeStr;

  double timeDiff = 0;

  if (isPaused) {
    end = pausedAt;
  }

  timeDiff = getTimeDiff(startedAt+pauseTotal, end);

  timeStr = toString(timeDiff);

  text->setContent(timeStr);
}


void Timer::update() {
  if (isPaused) {
    return;
  }
  updateDuration();
  draw();
}

int Timer::getPauseState() {
  return isPaused;
}
