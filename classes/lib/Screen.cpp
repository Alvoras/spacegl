#ifdef __APPLE__
  #include <GLUT/glut.h>
#elif __linux__
  #include <X11/Xlib.h>
  #include <GL/gl.h>
  #include <GL/glut.h>
  #include <GL/glx.h>
#elif _WIN32
  #include <windows.h>
  #include <GL/glut.h>
#else
  #warning "OS or compiler not supported"
#endif

#include <stdlib.h>
#include <stdio.h>

#include "../../headers/toolbox.hpp"
#include "../../headers/config.hpp"
#include "../headers/Camera.hpp"
#include "../headers/Screen.hpp"

extern float _tmpGlobal1;

Screen::Screen(std::string title, long int interval, float r, float g, float b){
  text = new BlinkingText(title, interval, r, g, b);
  text->start();
  isVisible = false;
}

void Screen::show(){
   isVisible = true;
}

void Screen::hide(){
   isVisible = false;
}

bool Screen::getIsVisible(){
  return isVisible;
}

void Screen::drawBackground() {
  glBegin(GL_POLYGON);
  glColor4f(1,1,1,0.5);

  float x = 1.5;
  float y = 1.5;
  float z = 1;

  glTexCoord2f(-x, -y);
  glVertex3f(-x, -y, -z);
  glTexCoord2f(-x, y);
  glVertex3f(-x, y, -z);
  glTexCoord2f(x, y);
  glVertex3f(x, y, -z);
  glTexCoord2f(x, -y);
  glVertex3f(x, -y, -z);

  glEnd();
}

void Screen::draw() {
  Camera *camera = Camera::getInstance();
  float eyeX = camera->getEye(X);
  float eyeY = camera->getEye(Y);
  float eyeZ = camera->getEye(Z);

  if (isVisible) {
    text->draw(eyeX-0.3, eyeY, eyeZ-5);
    // drawBackground();
  }
}
