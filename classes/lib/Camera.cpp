#ifdef __APPLE__
  #include <GLUT/glut.h>
#elif __linux__
  #include <X11/Xlib.h>
  #include <GL/gl.h>
  #include <GL/glut.h>
  #include <GL/glx.h>
#elif _WIN32
  #include <windows.h>
  #include <GL/glut.h>
#else
  #warning "OS or compiler not supported"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "../headers/Game.hpp"
#include "../headers/Ship.hpp"
#include "../headers/GUI.hpp"
#include "../headers/Camera.hpp"

extern float _tmpGlobal1;

Camera Camera::camInstance;

Camera::Camera(){

}

Camera* Camera::getInstance() {
   return &camInstance;
}

void Camera::lookAt(){
  Ship *ship = Ship::getInstance();
  GUI *gui = GUI::getInstance();
  Game *game = Game::getInstance();
  float cameraOffset = 0;
  float cockpitOffsetBack = 0.42;
  float cockpitOffsetFront = 0.3;

  if ((cameraOffset = presentDropFront()) >= 0) {
    if (cockpitOffsetFront > 0) {
      cockpitOffsetFront -= 0.05;
      ship->setCockpitOffsetFront(cockpitOffsetFront);
    }
    if ((cameraOffset = presentBack()) <= 0) {
      if (cockpitOffsetBack > 0) {
        cockpitOffsetBack -= 0.05;
        ship->setCockpitOffsetBack(cockpitOffsetBack);
      }
      if ((cameraOffset = presentFront()) <= 0) {

        if ((cameraOffset = presentDropIn()) >= 0) {

          if (ship->isLocked()) {
            // gui->showGo();
            ship->setControlLock(false);
          }

          if (game->getIsPresenting()) {
            game->setIsPresenting(false);
          }

          if (!gui->getIsVisible()) {
            gui->show();
          }

          gluLookAt(eyeX, eyeY-cameraOffset, eyeZ-eyeZModifier-cameraOffset, focusX, focusY, focusZ, upX, upY, upZ);
        }else{
          // Will set the start time at the the as now() at each loop, hence resulting in a 0 time difference with now()
          // See Timer::updateDuration()
          gluLookAt(eyeX, eyeY-cameraOffset, eyeZ-eyeZModifier-cameraOffset, focusX, focusY, focusZ, upX, upY, upZ);

          // Will set the start time at the the as now() at each loop, hence resulting in a 0 time difference with now()
          // See Timer::updateDuration()
          gui->startTimer();
        }
      }else{
        gluLookAt(eyeX-cameraOffset, eyeY, (eyeZ-eyeZModifier)*(-1), focusX-cameraOffset, focusY, focusZ, upX, upY, -upZ);

        // Will set the start time at the the as now() at each loop, hence resulting in a 0 time difference with now()
        // See Timer::updateDuration()
        gui->startTimer();
      }
  }else{
    gluLookAt(eyeX-cameraOffset, eyeY, eyeZ-eyeZModifier, focusX-cameraOffset, focusY, focusZ, upX, upY, -upZ);

    // Will set the start time at the the as now() at each loop, hence resulting in a 0 time difference with now()
    // See Timer::updateDuration()
    gui->startTimer();
  }

  }else{
    gluLookAt(eyeX-cameraOffset, eyeY, (eyeZ-eyeZModifier-cameraOffset)*(-1), focusX-cameraOffset, focusY, focusZ, upX, upY, -upZ);

    // Will set the start time at the the as now() at each loop, hence resulting in a 0 time difference with now()
    // See Timer::updateDuration()
    gui->startTimer();
  }
}

float Camera::presentDropIn(){
  static float eyeYCounter = -3.5;

  if (eyeYCounter < 0) {
    eyeYCounter += 0.02;
  }

  return eyeYCounter;
}

float Camera::presentDropFront(){
  static float eyeXCounter = -2;

  if (eyeXCounter < 0) {
    eyeXCounter += 0.01;
  }

  return eyeXCounter;
}

float Camera::presentFront(){
  static float eyeXCounter = 4;

  if (eyeXCounter > 0) {
    eyeXCounter -= 0.02;
  }

  return eyeXCounter;
}

float Camera::presentBack(){
  static float eyeXCounter = 4;

  if (eyeXCounter > 0) {
    eyeXCounter -= 0.02;
  }

  return eyeXCounter;
}

void Camera::update(){
      static int reset = 0;

      switch(view)
      {
      case CENTRAL_VIEW:     //centré
          if(reset == 0)
          {
              focusX = 0;
              focusY = 0;
              focusZ = 0;

              reset = 1;
          }

          eyeX = r * sin(alpha) * cos(beta);
          eyeY = r * sin(beta);
          eyeZ = r * cos(alpha) * cos(beta);
          break;

      case FREE_VIEW:     //Libre
          if(reset == 1)
          {
              alpha = 0;
              beta = 0;
              eyeX = 0;
              eyeY = 0;
              eyeZ = 0;
              r = 5;

              reset = 0;
          }

          focusX = r * sin(alpha) * cos(beta) + eyeX;
          focusY = r * sin(beta) + eyeY;
          focusZ = -1 * r * cos(alpha) * cos(beta) + eyeZ;
          break;
      }
}

void Camera::unaryEye(int id, int mode){
  switch (id) {
    case X:
      eyeX++;
    break;

    case Y:
      eyeY++;
    break;

    case Z:
      eyeZ++;
    break;
  }
}

void Camera::unaryFocus(int id, int mode){
  switch (id) {
    case X:
      focusX++;
    break;

    case Y:
      focusY++;
    break;

    case Z:
      focusZ++;
    break;
  }
}

void Camera::unaryUp(int id, int mode){
  switch (id) {
    case X:
      upX++;
    break;

    case Y:
      upY++;
    break;

    case Z:
      upZ++;
    break;
  }
}

GLdouble Camera::getEye(int id){
  switch (id) {
    case X:
      return eyeX;
    break;

    case Y:
      return eyeY;
    break;

    case Z:
      return eyeZ;
    break;

    default:
    return 0;
  }
}

GLdouble Camera::getFocus(int id){
  switch (id) {
    case X:
      return focusX;
    break;

    case Y:
      return focusY;
    break;

    case Z:
      return focusZ;
    break;

    default:
    return 0;
  }
}

GLdouble Camera::getUp(int id){
  switch (id) {
    case X:
      return upX;
    break;

    case Y:
      return upY;
    break;

    case Z:
      return upZ;
    break;

    default:
    return 0;
  }
}

void Camera::setEye(int id, GLdouble val){
  switch (id) {
    case X:
      eyeX = val;
    break;

    case Y:
      eyeY = val;
    break;

    case Z:
      eyeZ = val;
    break;
  }
}

void Camera::setEyeModifier(int id, GLdouble val){
  switch (id) {
    case X:
      eyeXModifier = val;
    break;

    case Y:
      eyeYModifier = val;
    break;

    case Z:
      eyeZModifier = val;
    break;
  }
}

GLdouble Camera::getEyeModifier(int id){
  switch (id) {
    case X:
    return eyeXModifier;
    break;

    case Y:
    return eyeYModifier;
    break;

    case Z:
    return eyeZModifier;
    break;
    default:

    return 0;
  }
}

void Camera::setFocus(int id, GLdouble val){
  switch (id) {
    case X:
      focusX = val;
    break;

    case Y:
      focusY = val;
    break;

    case Z:
      focusZ = val;
    break;
  }
}

void Camera::setUp(int id, GLdouble val){
  switch (id) {
    case X:
      upX = val;
    break;

    case Y:
      upY = val;
    break;

    case Z:
      upZ = val;
    break;
  }
}

GLdouble Camera::getR(){
  return r;
}

GLdouble Camera::getAlpha(){
  return alpha;
}

GLdouble Camera::getBeta(){
  return beta;
}
