#ifdef __APPLE__
  #include <GLUT/glut.h>
#elif __linux__
  #include <X11/Xlib.h>
  #include <GL/gl.h>
  #include <GL/glut.h>
  #include <GL/glx.h>
#elif _WIN32
  #include <windows.h>
  #include <GL/glut.h>
#else
  #warning "OS or compiler not supported"
#endif

#include <iostream>
#include <stdlib.h>
#include <stdio.h>

#include "classes/headers/Game.hpp"

#include "headers/config.hpp"
#include "headers/glut_tools.hpp"
#include "headers/geometry.hpp"
#include "headers/toolbox.hpp"

int screenW = 888;
int screenH = 500;
Asteroid *firstAsteroid;

/* Programme principal */
int main(int argc, char **argv){
        Game *game = Game::getInstance();
        unsigned int fd = game->getFrameDelay();

        /* Initialisation de glut et creation de la fenetre */
        glutInit(&argc, argv);
        glutSetKeyRepeat(GLUT_KEY_REPEAT_OFF);
        glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA | GLUT_DEPTH);
        glutInitWindowSize(screenW, screenH);
        glutInitWindowPosition (100, 100);
        glutCreateWindow("SpaceGL");
        glutSetCursor(GLUT_CURSOR_NONE);

        /* Initialisation d'OpenGL */
        initRendering();

        /* Enregistrement des fonctions de rappel */
        glutDisplayFunc(display);
        glutReshapeFunc(reshape);
        glutKeyboardFunc(keyboard);
        glutKeyboardUpFunc(keyboardUp);
        glutSpecialFunc(specialInput);

        glutPassiveMotionFunc(mouseMove);

        initAsteroid();
        addAsteroid();

        glutTimerFunc(fd, update, 0);

        /* Entre dans la boucle principale de glut, traitement des vnements */
        glutMainLoop();
        return 0;
}
