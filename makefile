CC=g++
CFLAGS= -Wall
EXEC=spacegl
LINKER=-lglut32 -lSOIL -lopengl32 -lglu32 -lwinmm -lgdi32 -lm
OBJ=main.cpp lib/glut_tools.cpp lib/geometry.cpp lib/toolbox.cpp classes/lib/Light.cpp classes/lib/Ship.cpp classes/lib/Game.cpp classes/lib/Screen.cpp classes/lib/Timer.cpp classes/lib/GUI.cpp classes/lib/Camera.cpp classes/lib/Text.cpp classes/lib/BlinkingText.cpp classes/lib/Skybox.cpp
CLR=cls

# Windows
ifdef SYSTEMROOT
	CLR=cls
	EXT=.exe
else
	# Linux
	ifeq ($(shell uname), Linux)
		CLR=clear
		LINKER=-lGL -lGLU -lglut -lm
		PRE=./
	endif
endif

all: $(OBJ)
#	$(CLR) && $(CC) $(CFLAGS) $(OBJ) $(LINKER) -o $(PRE)$(EXEC)$(EXT)
	$(CC) $(CFLAGS) $(OBJ) $(LINKER) -o $(PRE)$(EXEC)$(EXT)

run:	$(OBJ)
	$(CLR) && $(CC) $(CFLAGS) $(OBJ) $(LINKER) -o $(EXEC) && $(PRE)$(EXEC)$(EXT)
